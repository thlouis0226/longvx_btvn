package com.example.demo.controller;

import com.example.demo.model.Employee;
import com.example.demo.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
public class EmployeeController{
    @Autowired
    private IEmployeeService iEmployeeService;
    @GetMapping("/")
    public String test() {
        return "haha";
    }
    @PostMapping("/add")
    public Employee addEmployee(@RequestBody Employee employee){
        return iEmployeeService.addEmployee(employee);
    }
    @PutMapping("/update")
    public Employee updateEmployee(@RequestParam("id") long id, @RequestBody Employee employee){
        return iEmployeeService.updateEmployee(id, employee);
    }
    @DeleteMapping("/delete")
    public boolean deleteEmployee(@RequestParam("id") long id){
        return iEmployeeService.deleteEmployedd(id);
    }
    @GetMapping("/list")
    public List<Employee> getAllEmployee(){
        return iEmployeeService.getAllEmployee();
    }
    @GetMapping("/getone")
    public Optional<Employee> getone(long id){
        return iEmployeeService.getOneEmployee(id);
    }
}

