package com.example.demo.service;

import com.example.demo.model.Employee;

import java.util.List;
import java.util.Optional;

public interface IEmployeeService {
    //Them nhan vien
    public Employee addEmployee(Employee employee);
    //Chinh sua thong tin nhan vien
    public Employee updateEmployee(long id, Employee employee);
    //xoa nhan vien
    public boolean deleteEmployedd(long id);
    //ham lay ra ds
    public List<Employee> getAllEmployee();
    //Ham lay ra 1 nhan vien
    public Optional<Employee> getOneEmployee(long id);
}
