package com.example.demo.service;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeImpl implements IEmployeeService {
    @Autowired
    private EmployeeRepository employeeres;
    @Override
    public Employee addEmployee(Employee employeee) {
        if(employeee!=null) {
            return employeeres.save(employeee);
        }
        return null;
    }

    @Override
    public Employee updateEmployee(long id, Employee employee) {
        if(employee != null) {
            Employee employee2 = employeeres.getById(id);
            if(employee2!=null) {
                employee2.setName(employee.getName());
                employee2.setAge(employee.getAge());
                return employeeres.save(employee2);
            }
        }
        return null;
    }

    @Override
    public boolean deleteEmployedd(long id) {
        if(id>=1) {
            Employee employee = employeeres.getById(id);
            if(employee!=null) {
                employeeres.delete(employee);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Employee> getAllEmployee() {
        // TODO Auto-generated method stub
        return employeeres.findAll();
    }

    @Override
    public Optional<Employee> getOneEmployee(long id) {
        return employeeres.findById(id);
    }

}
